﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kargo
{
    public partial class KargoGiris : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnkydet_Click(object sender, EventArgs e)
        {
            List<kargogiris> liste = new List<kargogiris>();
            SqlConnection baglan = new SqlConnection(ConfigurationManager.ConnectionStrings["baglan"].ToString());
            baglan.Open();
            SqlCommand komut = new SqlCommand();
            komut.Connection = baglan;
            komut.CommandText = "kargogirisSP";
            komut.CommandType = CommandType.StoredProcedure;
            komut.Parameters.AddWithValue("@GAdi", TextBox1.Text);
            komut.Parameters.AddWithValue("@GSoyad", TextBox2.Text);
            komut.Parameters.AddWithValue("@AAdi", TextBox3.Text);
            komut.Parameters.AddWithValue("@ASoyad", TextBox4.Text);
            komut.Parameters.AddWithValue("@GAdres", TextBox5.Text);
            komut.Parameters.AddWithValue("@AAdres", TextBox6.Text);
            komut.Parameters.AddWithValue("@Agirlik", TextBox7.Text);
            komut.Parameters.AddWithValue("@Tur", TextBox8.Text);
            komut.Parameters.AddWithValue("@OdemeTuru", TextBox9.Text);
            komut.Parameters.AddWithValue("@Fiyat", TextBox10.Text);
            komut.ExecuteNonQuery();
            baglan.Close();
        }

        protected void btnanasayfa_Click(object sender, EventArgs e)
        {
            Response.Redirect("anasayfa.aspx");
        }

       

       
    }
}