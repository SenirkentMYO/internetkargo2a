﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Kargo
{
    public class Kulislemleri
    {
        public kullanici Giris(string email, string sifre)
        {

            List<kullanici> liste = new List<kullanici>();
            SqlConnection baglanti = new SqlConnection(ConfigurationManager.ConnectionStrings["baglan"].ToString());
            baglanti.Open();
            SqlCommand komut = new SqlCommand();
            komut.Connection = baglanti;
            komut.CommandType = CommandType.StoredProcedure;
            komut.CommandText = "kullanicigirisSP";

            SqlParameter p1 = new SqlParameter();
            p1.DbType = DbType.String;
            p1.ParameterName = "@GelenEmail";
            p1.Value = email;


            SqlParameter p2 = new SqlParameter();
            p2.DbType = DbType.String;
            p2.ParameterName = "@GelenSifre";
            p2.Value = sifre;


            komut.Parameters.Add(p1);
            komut.Parameters.Add(p2);


            SqlDataReader satir = komut.ExecuteReader();
            while (satir.Read())
            {

            }


            if (liste.Count > 0)
            {
                return liste[0];
            }
            else
            {
                return null;
            }
        }
    }
}